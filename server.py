import asyncio, websockets, numpy, cv2, socket
from PIL import Image

import lib.matcher as mt, lib.exchanger as change

class WebSockets_Server:

    def __init__(self, loop, address , port):
        self.loop = loop
        self.address = address
        self.port = port


    async def _handler(self, websocket, path):
        step_frame = numpy.array([])
        while True:
            recv_data = await websocket.recv()
            image = change.byte_to_image(recv_data.split(',')[1])
            # 表示したいときにcomment外してね
            # cv2.imshow('', image)
            print(mt.detMatch(image, step_frame))
            # print(mt.histMatch(image, step_frame))
            key = cv2.waitKey(1)
            if key == 27: # esc
                break
            step_frame = image

    
    def run(self):
        self._server = websockets.serve(self._handler, self.address, self.port)
        self.loop.run_until_complete(self._server)
        self.loop.run_forever()

if __name__ == '__main__':
    PORT = 8889
    loop = asyncio.get_event_loop()
    wss = WebSockets_Server(loop, '0.0.0.0', PORT)
    print(
        '\n\n########################################################\n\n' +
        '\tOpen (main) : "ws://' + socket.gethostbyname(socket.gethostname()) + ':' + str(PORT) + '"\n'+
        '\tOpen (stub) : "ws://localhost:' + str(PORT) + '"\n\n'+        
        '\tPROTOCOL    : ws(websocket)\n' +
        '\tIP ADDRESS  : ' + socket.gethostbyname(socket.gethostname()) + '\n' +
        '\tPORT        : ' + str(PORT)  + '\n\n'+
        '\t\t\tOtsukare by KAZUHIRO SEIDA.' 
        '\n\n########################################################'
    )
    wss.run()
