# Server for Video

## インストール手順🐢
 
```shell
    $ conda env create -f=packages.yml
    $ conda activate dev-md
    $ pip install opencv-python
```

## 実行手順🐇
```shell
    $ conda activate dev-md
    $ python server.py
```
👍👍👍👍👍👍👍👍👍👍👍👍👍👍👍👍👍👍👍👍👍👍
![./result.png](./.gitresources/result.png)

## ちなみに💛
サーバー停止はコンソール破棄、またはタスクマネージャーから落としてね。それかkillコマンド