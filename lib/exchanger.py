from PIL import Image
import cv2, numpy as np, base64, io

byte_to_image =  lambda byte: cv2.cvtColor(np.asarray( Image.open(io.BytesIO(base64.b64decode(byte)))), cv2.COLOR_RGB2BGR)
    