import numpy as np, cv2


bf = cv2.BFMatcher(cv2.NORM_HAMMING)
# detector = cv2.ORB_create()
detector = cv2.AKAZE_create()


def histMatch(img1, img2):
    img1 = cv2.calcHist([img1], [0], None, [256], [0, 256])
    img2 = cv2.calcHist([img2], [0], None, [256], [0, 256])    
    return cv2.compareHist(img1, img2, 0)

def detMatch(img1, img2):
    try:
        (img1_kp, img1_des) = detector.detectAndCompute(img1, None)
        (img2_kp, img2_des) = detector.detectAndCompute(img2, None)
        matches = bf.match(img1_des, img2_des)
        dist = [m.distance for m in matches]
        return sum(dist) / len(dist)
    except:
        return 99999999999